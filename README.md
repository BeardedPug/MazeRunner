# Mazerunner

A program to solve simple mazes. The mazes are given in a file and the program must read in the file, solve the maze and output the solution.
If no solution is possible the output will indicate this. 
  
  - Arbitrary sized mazes are handled
  - Valid moves are N, S, E, W (not diagonally)
  - All input are clean, no validation is necessary

#### Example:

```
INPUT FILE:
10 10
1 1
8 8
1 1 1 1 1 1 1 1 1 1
1 0 0 0 0 0 0 0 0 1
1 0 1 0 1 1 1 1 1 1
1 0 1 0 0 0 0 0 0 1
1 0 1 1 0 1 0 1 1 1
1 0 1 0 0 1 0 1 0 1
1 0 1 0 0 0 0 0 0 1
1 0 1 1 1 0 1 1 1 1
1 0 1 0 0 0 0 0 0 1
1 1 1 1 1 1 1 1 1 1

OUTPUT:
##########
#SXX     #
# #X######
# #XX    #
# ##X# ###
# # X# # #
# # XX   #
# ###X####
# #  XXXE#
##########
```

## How To Run
- Install git: [instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Make a github account (if you don't have one already)
- Clone this repository: ```git clone https://gitlab.com/BeardedPug/MazeRunner.git```
- Install gradle from [here](https://gradle.org)
- Open the terminal
- cd into MazeRunner
- Run ```gradle build```
- Run ```gradle fatjar```
- cd into build/libs
- Run ```java -jar MazeRunner-1.0.0.jar```