import ch.qos.logback.classic.Level;
import lombok.extern.slf4j.Slf4j;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

@Slf4j
public class MazeRunner {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        if (args.length == 0){
            root.setLevel(Level.INFO);
        } else {
            root.setLevel(Level.DEBUG);
        }

        log.debug("Maze Runner Started");
        selectMazeProcess();
        log.debug("Finished");
    }

    private static void solveMaze(String mazeFile) {
        Maze maze = new Maze().loadMazeFromFile(mazeFile);
        if (maze != null) {
            maze.solve();
            if (selectAnotherMaze()) {
                selectMazeProcess();
            }
        } else {
            System.out.println("Error loading: " + mazeFile);
            System.out.println("Please try again...");
            selectMazeProcess();
        }
    }

    private static void selectMazeProcess(){
        String answer = selectFromExisting();
        if (answer.equals("y")) {
            answer = selectedExisting();
        } else {
            answer = enterFileName();
        }
        solveMaze(answer);
    }

    private static String selectFromExisting(){
        System.out.println("Would you like to select from the list of pre-existing mazes (y/n)");
        String answer = scanner.next().toLowerCase();
        if (answer.equals("y") || answer.equals("n")) {
            return answer;
        } else {
            System.out.println("Please select either 'y' for yes or 'n' for no");
            return selectFromExisting();
        }
    }

    private static String selectedExisting(){
        String answer;
        String[] files = {"input.txt", "small.txt", "small_wrap_input.txt", "medium_input.txt", "large_input.txt"};
        System.out.println("Please select the number of the maze from the following:");
        for (int i = 0; i < files.length; i++) {
            System.out.println((i+1) + ": " + files[i]);
        }
        int selected = scanner.nextInt();
        if (selected > 0 && selected <= files.length) {
            answer = files[selected - 1];
        } else {
            System.out.println("Please enter a valid number.");
            return selectedExisting();
        }
        return answer;
    }

    private static String enterFileName(){
        System.out.println("Please enter the path of the maze file you wish to use:");
        return scanner.next();
    }

    private static boolean selectAnotherMaze(){
        System.out.println("Would you like to select another maze? (y/n)");
        String answer = scanner.next().toLowerCase();
        if (answer.equals("y")) {
            return true;
        } else if (answer.equals("n")) {
            return false;
        }  else {
            System.out.println("Please select either 'y' for yes or 'n' for no");
            return selectAnotherMaze();
        }
    }
}