import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MazeTest {

    private static Maze testMazeSolvable = new Maze();
    private static Maze testMazeUnsolvable = new Maze();

    @BeforeAll
    static void setup() {
        testMazeSolvable.setWidth(5);
        testMazeSolvable.setHeight(5);
        testMazeSolvable.setStartY(1);
        testMazeSolvable.setStartX(1);
        testMazeSolvable.setEndY(3);
        testMazeSolvable.setEndX(3);
        testMazeSolvable.setBoolMaze(new int[][]{
                {1, 1, 1, 1, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 0, 0, 1},
                {1, 1, 1, 1, 1}});
        testMazeSolvable.setCharMaze(new char[][]{
                {'#', '#', '#', '#', '#'},
                {'#', 'S', '#', ' ' , '#'},
                {'#', ' ', '#', ' ' , '#'},
                {'#', ' ', ' ', 'E' , '#'},
                {'#', '#', '#', '#', '#'}});

        testMazeUnsolvable.setWidth(5);
        testMazeUnsolvable.setHeight(5);
        testMazeUnsolvable.setStartY(1);
        testMazeUnsolvable.setStartX(1);
        testMazeUnsolvable.setEndY(3);
        testMazeUnsolvable.setEndX(3);
        testMazeUnsolvable.setBoolMaze(new int[][]{
                {1, 1, 1, 1, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1},
                {1, 1, 1, 1, 1}});
        testMazeUnsolvable.setCharMaze(new char[][]{
                {'#', '#', '#', '#', '#'},
                {'#', 'S', '#', ' ' , '#'},
                {'#', ' ', '#', ' ' , '#'},
                {'#', ' ', '#', 'E' , '#'},
                {'#', '#', '#', '#', '#'}});
    }

    @Test
    void loadMazeFromFileTest(){
        Maze loadFromFileTest = new Maze().loadMazeFromFile("TestMazes/oneTrack.txt");
        assert(loadFromFileTest.getWidth() == 5);
        assert(loadFromFileTest.getHeight() == 5);
        assert(loadFromFileTest.getStartY() == 1);
        assert(loadFromFileTest.getStartX() == 1);
        assert(loadFromFileTest.getEndY() == 3);
        assert(loadFromFileTest.getEndX() == 3);
        assert(loadFromFileTest.getBoolMaze()[1][1] == 0);
        assert(loadFromFileTest.getCharMaze()[1][1] == 'S');
        assert(loadFromFileTest.getCharMaze()[3][3] == 'E');
    }

    @Test
    void printMazeTest() {
        testMazeSolvable.printMaze();
    }

    @Test
    void solveBasicTest() {
        System.out.println("Positive test -");
        testMazeSolvable.solve();
        assertNotNull(testMazeSolvable.getOptimum());
        System.out.println("");
        System.out.println("Negative test -");
        testMazeUnsolvable.solve();
        assertNull(testMazeUnsolvable.getOptimum());
    }

    @Test
    void solveAdvancedTest() {
        Maze advancedMaze = new Maze();
        advancedMaze.loadMazeFromFile("TestMazes/twoTrack.txt");
        System.out.println("multiples options test -");
        advancedMaze.solve();
        assertNotNull(advancedMaze.getOptimum());
        System.out.println("");
        advancedMaze.loadMazeFromFile("TestMazes/open.txt");
        System.out.println("open maze options test -");
        advancedMaze.solve();
        assertNotNull(advancedMaze.getOptimum());
        System.out.println("");
        advancedMaze.loadMazeFromFile("TestMazes/large.txt");
        System.out.println("large maze test -");
        advancedMaze.solve();
        assertNotNull(advancedMaze.getOptimum());

        //If wanting the optimal route:
        //Suggestion: Move program to Go for greater/simpler concurrency control for maps like this. See https://gitlab.com/BeardedPug/Webcrawler

        //If wanting a route regardless of length:
        //Suggestion: Change to depth first search rather than breadth first.

        /*
        System.out.println("");
        advancedMaze.loadMazeFromFile("sparse_medium.txt");
        System.out.println("sparse test -");
        advancedMaze.solve();
        assertNotNull(advancedMaze.getOptimum());
        */
    }

    @Test
    void solveWrapTest() {
        Maze wrapMaze = new Maze();
        wrapMaze.loadMazeFromFile("TestMazes/eastWrap.txt");
        System.out.println("east test -");
        wrapMaze.solve();
        assertNotNull(wrapMaze.getOptimum());
        System.out.println("");
        wrapMaze.loadMazeFromFile("TestMazes/northWrap.txt");
        System.out.println("north test -");
        wrapMaze.solve();
        assertNotNull(wrapMaze.getOptimum());
        System.out.println("");
        wrapMaze.loadMazeFromFile("TestMazes/southWrap.txt");
        System.out.println("south test -");
        wrapMaze.solve();
        assertNotNull(wrapMaze.getOptimum());
        System.out.println("");
        wrapMaze.loadMazeFromFile("TestMazes/westWrap.txt");
        System.out.println("west test -");
        wrapMaze.solve();
        assertNotNull(wrapMaze.getOptimum());
        System.out.println("");
    }

    @Test
    void findOptimumSolutionTest() {
        assert(testMazeSolvable.findOptimumSolution(null).getCoordinates().size() == 5);
        assert(testMazeUnsolvable.findOptimumSolution(null).getCoordinates().size() == 1);
    }
}