import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
class Maze {

    private int width;
    private int height;
    private int startX;
    private int startY;
    private int endX;
    private int endY;
    private int[][] boolMaze;
    private char[][] charMaze;
    private Solution optimum;

    Maze loadMazeFromFile(String file) {
        Scanner input;
        try {
            String path = file;
            if (!Paths.get(file).isAbsolute()) {
                path = MazeRunner.class.getResource(file).toURI().getPath();
                if(path == null){
                    input = new Scanner(getClass().getResourceAsStream(file));
                } else {
                    input = new Scanner(new File(path));
                }
            } else {
                input = new Scanner(new File(path));
            }
        } catch (FileNotFoundException | NullPointerException e){
            log.info("No file found for: " + file);
            log.error(e.getMessage(), e);
            return null;
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e);
            return null;
        }

        this.setWidth(input.nextInt());
        this.setHeight(input.nextInt());

        this.setStartX(input.nextInt());
        this.setStartY(input.nextInt());

        this.setEndX(input.nextInt());
        this.setEndY(input.nextInt());

        int[][] numericMaze = new int[height][width];
        char[][] characterMaze = new char[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                numericMaze[y][x] = input.nextInt();
                if(numericMaze[y][x] == 1){
                    characterMaze[y][x] = '#';
                } else {
                    characterMaze[y][x] = ' ';
                }
            }
        }

        characterMaze[startY][startX] = 'S';
        characterMaze[endY][endX] = 'E';
        this.setBoolMaze(numericMaze);
        this.setCharMaze(characterMaze);
        log.debug(this.toString());
        return this;
    }

    void printMaze() {
        System.out.println();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                System.out.print(" " + charMaze[y][x]);
            }
            System.out.println();
        }
    }

    void solve() {
        optimum = findOptimumSolution(null);
        if (optimum.getLatestMove().getX() == endX && optimum.getLatestMove().getY() == endY) {
            for ( int i = 1; i < optimum.coordinates.size()-1; i++) {
                charMaze[optimum.coordinates.get(i).getY()][optimum.coordinates.get(i).getX()] = 'X';
            }
            printMaze();
        } else {
            System.out.print("No solution found");
            optimum = null;
        }
    }

    Solution findOptimumSolution(Solution currentSolution){
        Solution foundSolution;
        if (currentSolution == null) {
            foundSolution = new Solution();
            foundSolution.getCoordinates().add(new Coordinate(this.getStartX(), this.getStartY()));
        } else {
            foundSolution = currentSolution;
        }

        ArrayList<Solution> potentialSolutions = new ArrayList<>();
        for (Coordinate move: checkAvailableMoves(foundSolution).coordinates) {
           Solution potential = new Solution();
           potential.setCoordinates((ArrayList<Coordinate>) foundSolution.getCoordinates().clone());
           potential.coordinates.add(move);
           if (move.x == endX && move.y == endY) {
               return potential; // Return finished solution here as any others on this path shall be longer.
           } else {
               potentialSolutions.add(potential);
           }
        }

        ArrayList<Solution> finishedSolutions = new ArrayList<>();
        for (Solution potential: potentialSolutions) {
                finishedSolutions.add(findOptimumSolution(potential));
        }

        Solution winningSolution = null;
        for (Solution potentialWinner: finishedSolutions) {
            if (potentialWinner.getLatestMove().getX() == endX && potentialWinner.getLatestMove().getY() == endY) {
                if (winningSolution == null || potentialWinner.coordinates.size() < winningSolution.coordinates.size()) {
                    winningSolution = potentialWinner;
                }
            }
        }

        if (winningSolution == null) {
            winningSolution = foundSolution;
        }

        return winningSolution;
    }

    Solution checkAvailableMoves(Solution currentSolution) {
        Solution availableMoves = new Solution();
        Coordinate checkFrom = currentSolution.getLatestMove();
        //North
        if (coordinateIsAvailable(checkFrom.getNorth(), currentSolution)) {
            availableMoves.coordinates.add(checkFrom.getNorth());
        }
        //East
        if (coordinateIsAvailable(checkFrom.getEast(), currentSolution)) {
            availableMoves.coordinates.add(checkFrom.getEast());
        }
        //South
        if (coordinateIsAvailable(checkFrom.getSouth(), currentSolution)) {
            availableMoves.coordinates.add(checkFrom.getSouth());
        }
        //West
        if (coordinateIsAvailable(checkFrom.getWest(), currentSolution)) {
            availableMoves.coordinates.add(checkFrom.getWest());
        }

        return availableMoves;
    }

    boolean coordinateIsAvailable(Coordinate toCheck, Solution currentSolution) {
        if (toCheck.isCoordinateValid(this.getWidth(), this.getHeight())) {
            if (!currentSolution.hasCoordinate(toCheck)) {
                return boolMaze[toCheck.getY()][toCheck.getX()] == 0;
            }
        }
        return false;
    }

    @Setter @Getter
    @NoArgsConstructor
    class Solution {
        ArrayList<Coordinate> coordinates = new ArrayList<>();

        boolean hasCoordinate(Coordinate toCheck) {
            for (Coordinate coordinate: coordinates) {
                if (toCheck.getX() == coordinate.getX() && toCheck.getY() == coordinate.getY()) {
                    return true;
                }
            }
            return false;
        }

        Coordinate getLatestMove() {
            if (coordinates.size() > 0) {
                return coordinates.get(coordinates.size()-1);
            } else return null;
        }
    }

    @Setter @Getter
    @AllArgsConstructor
    class Coordinate {
        private int x, y;

        Coordinate getNorth() {
            if (y == 0) {
                return new Coordinate(x, height - 1);
            } else {
                return new Coordinate(x, y - 1);
            }
        }

        Coordinate getEast() {
            if (x == width - 1) {
                return new Coordinate(0, y);
            } else {
                return new Coordinate(x + 1, y);
            }
        }

        Coordinate getSouth() {
            if (y == height - 1) {
                return new Coordinate(x, 0);
            } else {
                return new Coordinate(x, y + 1);
            }
        }

        Coordinate getWest() {
            if (x == 0) {
                return new Coordinate(width - 1, y);
            } else {
                return new Coordinate(x - 1, y);
            }
        }

        boolean isCoordinateValid(int mazeWidth, int mazeHeight) {
            return x >= 0 && y >= 0 && x <= mazeWidth - 1 && y <= mazeHeight - 1;
        }
    }

}
